#!/usr/bin/env node
const { exec } = require('child_process')
const { getLastCommit } = require('git-last-commit')
const { readFileSync } = require('fs')

getLastCommit((err, { subject, author, branch }) => {
  if (err) {
    console.error(err)
    return
  }
  // if author is bitbucket it will loop
  console.log("Author is:", author.name)
  if (author.name === 'bitbucket-pipelines') return
  if (branch !== 'master') {
    const taggedBranch = branch.split('-').slice(0, 2).join('-')
    const firstSegment = taggedBranch.split('-')[0]
    const packageJsonVersion = JSON.parse(readFileSync('./package.json').toString()).version
    console.log({ taggedBranch, firstSegment, packageJsonVersion })
    if (packageJsonVersion.includes(firstSegment)) {
      console.log('Pre-releasing, version has already an rc tag')
      exec(`npm version prerelease -m '[skip ci] %s' && git push origin --follow-tags`)
    } else {
      console.log('Pre-patching, version has now rc')
      exec(`npm version prepatch --preid=${taggedBranch} -m '[skip ci] %s' && git push origin --follow-tags`)
    }
  } else {
    if (/^Merged in feature\//gm.test(subject)) {
      console.log('Versioning as minor', subject, branch, author)
      exec(`npm version minor -m '[skip ci] %s' && git push origin --follow-tags`)
    } else {
      console.log('Versioning as patch', subject, branch, author)
      exec(`npm version patch -m '[skip ci] %s' && git push origin --follow-tags`)
    }
  }
})